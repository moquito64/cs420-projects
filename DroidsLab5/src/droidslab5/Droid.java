package droidslab5;
import java.util.*;
import java.io.*;
/**
 *
 * @author Sebastian Blancette and Jordan Atwell
 */
public class Droid implements Comparable<Droid> {
    String droid;
    double price;
    String regex;
    
    //constructor
public Droid(String d, double p) {
    this.droid = d;
    this.price = p;
}

public String getName()
{
    return this.droid;
}

public String toString() {
    //System.out.println(this.droid + "\t" + this.price);
    return this.droid + "\t" + this.price;
}

public int compareTo(Droid dro) {
    if(this.price == dro.price)
    {
    return 0;
    }
    else if(this.price > dro.price)
    {
        return 1;
    }
    else
    {
        return -1;
    }

}





}
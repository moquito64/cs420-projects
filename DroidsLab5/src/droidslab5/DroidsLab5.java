package droidslab5;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import org.w3c.dom.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.text.DecimalFormat;


    /**
     * @param By Sebastian Blanchette and Jordan Atwell
     */
public class DroidsLab5
{ 
    //Lists of Droid Types
    static DecimalFormat dec = new DecimalFormat("#0.00");
    static List<Droid> R2 = new ArrayList();
    static List<Droid> R3 = new ArrayList();
    static List<Droid> R4 = new ArrayList();
    static List<Droid> R5 = new ArrayList();
    static List<Droid> P2 = new ArrayList();
    static List<Droid> R4P = new ArrayList();
    static List<Droid> BB = new ArrayList();
    static List<Droid> PO = new ArrayList();
    static List<Droid> TC = new ArrayList();
    static List<Droid> IG = new ArrayList();
    //Droid List
    static List<Droid> droidList = new ArrayList();        
    /*
    function to read in droids from a file
    ***edit this with any additions***
    */
    
    public static void read_droids(){
    File inf = new File("droids_in.txt");
     //RegEx for each droid type
            List<String> RegEx = new ArrayList(10);
            RegEx.add("R[2]-?[A-Z]\\d");
            RegEx.add("R[3]-?[A-Z]\\d");
            RegEx.add("R[4]-?[A-Z&&[^P]]\\d");
            RegEx.add("R[5]-?[A-Z]\\d");
            RegEx.add("P2-?[A-Z]\\d");
            RegEx.add("R4-P\\d?\\d");
            RegEx.add("BB-\\d([A-Z]\\d?)?");
            RegEx.add("[A-Z]\\d?-3PO");
            RegEx.add("TC-\\d{2}");
            RegEx.add("IG-[0-9[A-Z]]{2}");
            //Lists for all droid types
            
            String line;
            //ONE TIME!
            String regex = "((R[2-5]-?[A-Z]\\d|P2-?[A-Z]\\d|R4-P\\d{2}|"
                   + "BB-\\d([A-Z]\\d?)?|[A-Z]\\d?-3PO|"
                    + "TC-\\d{2}|IG-[0-9[A-Z]]{2}).*?(>\\d{3,5}\\.?\\d{2}|>\\d{3,5}))";
        try (Scanner inputFile = new Scanner(inf)) {
            
           
            
            Pattern pattern = Pattern.compile(regex);
           
            while(inputFile.hasNext())
            {   
                
                line = inputFile.nextLine();
                //System.out.println(line);
                Matcher matcher = pattern.matcher(line);
                while(matcher.find()) //needs to be WHILE loop else it wont grab multiple matches from a line.
                {   
                    
                    //System.out.println(line);
                    String droidName = (matcher.group(2)); //matcher.group(2) is the names of the droids
                    //System.out.print(droidName);
                    //(matcher.group(4)) //matcher.group(4) is the price of the droid
                        //this checks the price for a . and edits it
                        String p = (matcher.group(4).substring(1)); //pehraps pass this into an array or variable
                        double price = Double.parseDouble(p);
                        Droid d = new Droid(droidName, price);
                        droidList.add(d);
                        //System.out.println(droidName);
                    
                    
                   
                }
             
                Collections.sort(droidList); //sorts droidList
               
               
                
            }
        } catch (FileNotFoundException ex) {
          
        }//This captures most of the general terms. 
          for(int count=0; count < droidList.size(); count++)
                {
                    for(int count2=0; count2 < RegEx.size(); count2++)
                    {
                        Pattern pat = Pattern.compile(RegEx.get(count2));
                        Matcher mat = pat.matcher(droidList.get(count).droid);
                        while(mat.find())
                        {
                            if(RegEx.get(count2).equals("R[2]-?[A-Z]\\d"))
                            {
                                R2.add(droidList.get(count));
                              
                            }
                            
                            else if(RegEx.get(count2).equals("R[3]-?[A-Z]\\d"))
                            {
                                R3.add(droidList.get(count));
                            }
                            else if(RegEx.get(count2).equals("R[4]-?[A-Z&&[^P]]\\d"))
                            {
                                R4.add(droidList.get(count));
                                
                            }
                            else if(RegEx.get(count2).equals("R[5]-?[A-Z]\\d"))
                            {
                                R5.add(droidList.get(count));
                            }
                            
                            else if(RegEx.get(count2).equals("P2-?[A-Z]\\d"))
                            {
                                P2.add(droidList.get(count));
                            }
                            else if(RegEx.get(count2).equals("R4-P\\d?\\d"))
                            {
                                R4P.add(droidList.get(count));
                            }
                            else if(RegEx.get(count2).equals("BB-\\d([A-Z]\\d?)?"))
                            {
                                BB.add(droidList.get(count));
                            }
                            else if(RegEx.get(count2).equals("[A-Z]\\d?-3PO"))
                            {
                                PO.add(droidList.get(count));
                            }
                            else if(RegEx.get(count2).equals("TC-\\d{2}"))
                            {
                                TC.add(droidList.get(count));
                            }
                            else
                            {
                                IG.add(droidList.get(count));
                            }
                            
                            
                        } 
                    }
                } 
      
    }
    //NOT USED THIS TIME
    public static void writeToFile(List<Droid> d, PrintWriter p, String n)
    {
        int size = d.size();
        try{
            if(size >= 1)
            {
                p.println("*************************\n" + size + " " + "*************************\n" + n + " units available");
            }   for(Droid x:d){
                p.println(x.toString());
               
            }
             
        }catch (Exception ex){
        ex.printStackTrace();}
        p.println("\n*************************\n");
        }
 /*
    
    Helper function. Thats loops through the Array of Specific Droid and appends its data to a node.
    */
    public static void getDroidsinfo(List<Droid> d, Document doc, Element node){
     for (int i = 0; i < d.size(); i++) {
                              Element element = doc.createElement("Droid");
                              Element cost = doc.createElement("Price");
                              node.appendChild(element);
                   
                              
                              element.setAttribute("ID", d.get(i).droid);
                              element.appendChild(cost).setTextContent(String.valueOf(dec.format(d.get(i).price) +" Imperial Credits"));
                              
                              }
    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException 
    {
      read_droids();
    
      DocumentBuilderFactory icFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder icBuilder; //need to create XML DOM structure
              try {
            icBuilder = icFactory.newDocumentBuilder();
            Document doc = icBuilder.newDocument(); //the most called thing here
            Element mainRootElement = doc.createElementNS("","Droids");//root
            doc.appendChild(mainRootElement);
            
            Element Astromech = doc.createElement("Astromech"); //break it down to Number Series 2-5
                    Element RSeries = doc.createElement("R-Series");
                    Element R2Droids = doc.createElement("R2-Droids");
                    Element R3Droids = doc.createElement("R3-Droids");
                    Element R4Droids = doc.createElement("R4-Droids");
                    Element R5Droids = doc.createElement("R5-Droids");
                    

                    
                    Element PSeries = doc.createElement("P-Series");
                    Element R4PSeries = doc.createElement("R4-PSeries");
            Element BBSeries = doc.createElement("BB-Series");
            Element BBDroids = doc.createElement("BB-Droids");//this was only gonna be used for structured consitancy.
            
            Element Protocol = doc.createElement("Protocol");
                    Element ThreePO = doc.createElement("ThreePO"); //ITS NOT VALID OR WELL FORMED XML to start with - or a digit
                            //Element name = doc.createElement("Name");
                            //Element cost = doc.createElement("Cost");
                    Element TCSeries = doc.createElement("TC-Series");
            Element IGAssassin = doc.createElement("IG-Assassin");
            /*
            This is the XML Structure. 
            */
            mainRootElement.appendChild(Astromech);
                    Astromech.appendChild(RSeries);
                            RSeries.appendChild(R2Droids);
                            getDroidsinfo(R2,doc,R2Droids);//helper function 
      
                            RSeries.appendChild(R3Droids);
                            getDroidsinfo(R3,doc,R3Droids);
                            
                            RSeries.appendChild(R4Droids);
                            getDroidsinfo(R4,doc,R4Droids);
                            
                            RSeries.appendChild(R5Droids);
                           getDroidsinfo(R5,doc,R5Droids);
                    Astromech.appendChild(PSeries);
                    getDroidsinfo(P2,doc,PSeries);
                    Astromech.appendChild(R4PSeries);
                    getDroidsinfo(R4P,doc,R4PSeries);
            mainRootElement.appendChild(BBSeries);
                    //BBSeries.appendChild(BBDroids);
                    getDroidsinfo(BB,doc,BBSeries);
            mainRootElement.appendChild(Protocol);
                    Protocol.appendChild(ThreePO);
                    getDroidsinfo(PO,doc,ThreePO);
                    Protocol.appendChild(TCSeries);
                    getDroidsinfo(TC,doc,TCSeries);
            mainRootElement.appendChild(IGAssassin);
            getDroidsinfo(IG,doc,IGAssassin);
            
            
 
            // output DOM XML to console and file
            //this is for readability
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes"); 
            DOMSource source = new DOMSource(doc);
            StreamResult console = new StreamResult(System.out);
            StreamResult file = new StreamResult(new File("droids_out.xml"));//file name
            transformer.transform(source, console);//for sanity 
            transformer.transform(source, file);//to file
            System.out.println("\nXML DOM Created Successfully..");
            
 
        } catch (Exception e) {
            e.printStackTrace();
        }
    
 
     
      
    
    }
}